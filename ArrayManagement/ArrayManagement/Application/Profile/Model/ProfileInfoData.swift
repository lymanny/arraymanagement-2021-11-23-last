//
//  ProfileInfoData.swift
//  ArrayManagement
//
//  Created by lymanny on 12/11/21.
//

import Foundation

// For title management
enum MyInfoSection : String{
    case Privacy
    case BizPoint
    case CancelReceipt
}


struct CustomMyInfoData<T> {
    var title  : String   // for section
    var value  : T?       // data in each section
}

struct CustomMyInfo {
    
    // Privacy
    struct Privacy {
        var title  : String  // for header title
        var object : [object]
        
        struct object {
            let title : String?
            let value : String?
        }
    }
    
    // Bizpoint
    struct Bizpoint {
        var title    : String
        var subTitle : String
        var object   : [object]
        
        struct object {
            let title     : String?
            let bizPoint  : String?
        }
    }
    
    // Receipt
    struct CancelReceipt {
        var title    : String
        var object   : [object]
        
        struct object {
            var name         : String?
            var price        : String?
            var date         : String?
            var id           : String?
            var objectDetail : objectDetail
            
            struct objectDetail {
                var approval_number : String?
                var supply_amount   : String?
                var vat             : String?
            }
        }
    }
    
}
