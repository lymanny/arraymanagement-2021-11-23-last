//
//  BizPointTableViewCell.swift
//  ArrayManagement
//
//  Created by lymanny on 12/11/21.
//

import UIKit

class BizPointTableViewCell: UITableViewCell {

    @IBOutlet weak var lblValue    : UILabel!
    @IBOutlet weak var lblTitle    : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configBizPointCell(subTitle: String, bizPointRec: CustomMyInfo.Bizpoint.object) {
        lblSubTitle.text = subTitle
        lblTitle.text    = bizPointRec.title
        lblValue.text    = bizPointRec.bizPoint
    }

}
