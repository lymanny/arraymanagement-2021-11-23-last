//
//  CancelReceiptTableViewCell.swift
//  ArrayManagement
//
//  Created by lymanny on 12/11/21.
//

import UIKit

class CancelReceiptTableViewCell: UITableViewCell {

    @IBOutlet weak var lblType   : UILabel!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var lblID     : UILabel!
    @IBOutlet weak var lblDate   : UILabel!
    @IBOutlet weak var lblPrice  : UILabel!
    @IBOutlet weak var lblName   : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCancelReceipt(cancelReceieptRec: CustomMyInfo.CancelReceipt.object) {
        lblName.text  = cancelReceieptRec.name
        lblDate.text  = cancelReceieptRec.date
        lblID.text    = cancelReceieptRec.id
        lblPrice.text = cancelReceieptRec.price
    }
    
    @IBAction func cancelBtnClick(_ sender: Any) {

    }
    
}
