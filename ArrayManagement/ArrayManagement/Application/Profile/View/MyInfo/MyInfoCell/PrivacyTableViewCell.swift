//
//  MyInfoTableViewCell.swift
//  ArrayManagement
//
//  Created by lymanny on 12/11/21.
//

import UIKit

class PrivacyTableViewCell: UITableViewCell {

    @IBOutlet weak var view     : UIView!
    @IBOutlet weak var lblValue : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configPrivacyCell(privacyRec: CustomMyInfo.Privacy.object) {
        lblTitle.text = privacyRec.title
        lblValue.text = privacyRec.value
    }
    

}
