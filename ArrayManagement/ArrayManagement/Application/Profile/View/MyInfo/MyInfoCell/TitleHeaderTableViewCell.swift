//
//  TitleHeaderTableViewCell.swift
//  ArrayManagement
//
//  Created by lymanny on 12/11/21.
//

import UIKit

class TitleHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitleHeader : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configHeaderCell(headerTitle: String) {
        lblTitleHeader.text = headerTitle
    }

}
