//
//  MyInformationVC.swift
//  ArrayManagement
//
//  Created by lymanny on 12/11/21.
//

import UIKit

class MyInformationVC: UIViewController {
    
    //MARK: - @IBOutlet
    @IBOutlet weak var tableView   : UITableView!
    @IBOutlet weak var viewProfile : UIView!
    
    //MARK: - variable & properties
    var myInfoVM = ProfileInfoDataVM()
    
    //MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // radious viewProfile
        viewProfile.layer.cornerRadius  = 40
        
        configTableView()
        
        myInfoVM.getMyInfoData()
        
        print("==>myInfoData", myInfoVM.myInfoData)
    }
    
    func configTableView(){
        // Disable Tableview Top Only
        tableView.bounces               = false
        tableView.alwaysBounceVertical  = false
        
        // Space between tableView & LogOutView
        self.tableView.contentInset     = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
    }
    
    //MARK: - Function
    @objc func cancelBtnClick() {
        print("work")
    }
    
    //MARK: - @IBAction
    @IBAction func signOutClick(_ sender: Any) {
        
        let alert = UIAlertController(title: "Do you want to sign out?", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: { action in
            print("No!")
        }))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            print("Yes!")
        }))
        
        self.present(alert, animated: true)
    }
    
}

//MARK: - UITableViewDataSource, UITableViewDelegate
extension MyInformationVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return myInfoVM.myInfoData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // row of title & object value
        let section = MyInfoSection(rawValue: myInfoVM.myInfoData[section].title)
        
        switch section {
        
        case .Privacy:
            return (myInfoVM.privacyRec?.object.count ?? 0) + 1 // for header title
        
        case .BizPoint:
            return (myInfoVM.bizpointRec?.object.count ?? 0) + 1 // for header title
        
        case .CancelReceipt:
            return (myInfoVM.cancelReceiptRec?.object.count ?? 0) + 1 // for header title
        
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = MyInfoSection(rawValue: myInfoVM.myInfoData[indexPath.section].title)
        
        switch section {
        
        case .Privacy:
            if indexPath.row == 0 {
                let headerTitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderTableViewCell") as! TitleHeaderTableViewCell
                headerTitleCell.configHeaderCell(headerTitle: myInfoVM.privacyRec?.title ?? "")
                return headerTitleCell
            }else {
                let privacyCell = tableView.dequeueReusableCell(withIdentifier: "PrivacyTableViewCell") as! PrivacyTableViewCell
//                privacyCell.configPrivacyCell(privacyRec: myInfoVM.privacyRec?.object[indexPath.row - 1] ?? CustomMyInfo.Privacy.object(title: "", value: ""))
                if let rec = myInfoVM.privacyRec?.object {
                    privacyCell.configPrivacyCell(privacyRec: rec[indexPath.row - 1])
                }
                return privacyCell
            }
            
        case .BizPoint:
            if indexPath.row == 0 {
                let headerTitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderTableViewCell") as! TitleHeaderTableViewCell
                headerTitleCell.configHeaderCell(headerTitle: myInfoVM.bizpointRec?.title ?? "")
                return headerTitleCell
            } else {
                let bizPointCell = tableView.dequeueReusableCell(withIdentifier: "BizPointTableViewCell") as! BizPointTableViewCell
                
                if let rec = myInfoVM.bizpointRec?.object {
                    bizPointCell.configBizPointCell(subTitle: myInfoVM.bizpointRec?.subTitle ?? "", bizPointRec: rec[indexPath.row - 1])
                }
                return bizPointCell
            }
            
            
        case .CancelReceipt:
            if indexPath.row == 0 {
                let headerTitleCell = tableView.dequeueReusableCell(withIdentifier: "TitleHeaderTableViewCell") as! TitleHeaderTableViewCell
                headerTitleCell.configHeaderCell(headerTitle: myInfoVM.cancelReceiptRec?.title ?? "")
                return headerTitleCell
            } else {
                let cancelReceiptCell = tableView.dequeueReusableCell(withIdentifier: "CancelReceiptTableViewCell") as! CancelReceiptTableViewCell
                
                if let rec = myInfoVM.cancelReceiptRec?.object {
                    cancelReceiptCell.configCancelReceipt(cancelReceieptRec: rec[indexPath.row - 1])
                }
                
                cancelReceiptCell.btnCancel.addTarget(self, action: #selector(cancelBtnClick), for: .touchUpInside)
                
                return cancelReceiptCell
            }
            
        default:
            return UITableViewCell()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let section = MyInfoSection(rawValue: myInfoVM.myInfoData[indexPath.section].title)
        
        switch section {
        case .CancelReceipt:
            
            // segue id : gotoReceiptDetailSegue
//            self.performSegue(withIdentifier: "gotoReceiptDetailSegue", sender: indexPath.row)
            guard let selectData = myInfoVM.cancelReceiptRec?.object[indexPath.row - 1] else {return}   // -1 for header
            
            let receiptDetailVC = storyboard?.instantiateViewController(identifier: "RecieptVC") as! RecieptVC
            receiptDetailVC.modalPresentationStyle = .fullScreen
            receiptDetailVC.selectReceiptData      = selectData
            self.present(receiptDetailVC, animated: true, completion: nil)
            
        default:
            break
        }
    }
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        if identifier == "gotoReceiptDetailSegue"{
            
            
        }
    }
    
    
}
