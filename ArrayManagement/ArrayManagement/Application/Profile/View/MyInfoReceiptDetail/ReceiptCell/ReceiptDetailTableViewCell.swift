//
//  ReceiptDetailTableViewCell.swift
//  IBK_BizCard_V2
//
//  Created by Ly Manny on 3/23/21.
//


import UIKit

class ReceiptDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblVat            : UITextField!
    @IBOutlet weak var lblSupplyAmount   : UILabel!
    @IBOutlet weak var lblApprovalAmount : UILabel!
    @IBOutlet weak var lblApprovalNumber : UILabel!
    @IBOutlet weak var lblID             : UILabel!
    @IBOutlet weak var lblDate           : UILabel!
    @IBOutlet weak var lblName           : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configDetailReceipt(detailReceiptRec: CustomMyInfo.CancelReceipt.object) {
        lblName.text           = detailReceiptRec.name
        lblDate.text           = detailReceiptRec.date
        lblID.text             = detailReceiptRec.id
        lblApprovalNumber.text = detailReceiptRec.objectDetail.approval_number
        lblApprovalAmount.text = detailReceiptRec.price
        lblSupplyAmount.text   = detailReceiptRec.objectDetail.supply_amount
        lblVat.text            = detailReceiptRec.objectDetail.vat
    }
    
}
