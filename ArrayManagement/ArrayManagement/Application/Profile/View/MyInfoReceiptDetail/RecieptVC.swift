//
//  RecieptVC.swift
//  ArrayManagement
//
//  Created by lymanny on 16/11/21.
//

import UIKit

class RecieptVC: UIViewController {

    //MARK: - @IBOutlet
    @IBOutlet weak var receiptTableView: UITableView!
    
    //MARK: - variable & properties
    var selectReceiptData  : CustomMyInfo.CancelReceipt.object?
    
    //MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("==>selectReceiptData", selectReceiptData)
    }
    
    //MARK: - @IBAction
    @IBAction func btnClose(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    //MARK: - function
    func registerCell() {
        receiptTableView.register(UINib(nibName: "ReceiptDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "ReceiptDetailTableViewCell")
    }

}

//MARK: - UITableViewDataSource
extension RecieptVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let receiptDetailCell = tableView.dequeueReusableCell(withIdentifier: "ReceiptDetailTableViewCell", for: indexPath) as! ReceiptDetailTableViewCell
        
        if let recDetail = selectReceiptData {
            receiptDetailCell.configDetailReceipt(detailReceiptRec: recDetail)
        }
        return receiptDetailCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 340
    }
    
    
}
