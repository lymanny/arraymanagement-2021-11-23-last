//
//  ProfileInfoDataVM.swift
//  ArrayManagement
//
//  Created by lymanny on 16/11/21.
//

import Foundation

class ProfileInfoDataVM {
    
    var myInfoData       = [CustomMyInfoData<Any>]()
    var privacyRec       : CustomMyInfo.Privacy?
    var bizpointRec      : CustomMyInfo.Bizpoint?
    var cancelReceiptRec : CustomMyInfo.CancelReceipt?
    
    
    func getMyInfoData() {
        // MARK: -  Init Privacy -
        privacyRec = CustomMyInfo.Privacy(title  : "Privacy",
                                          object : [
                                            CustomMyInfo.Privacy.object(title: "Name",            value: "Jisoo"),
                                            CustomMyInfo.Privacy.object(title: "User ID",         value: "181035"),
                                            CustomMyInfo.Privacy.object(title: "DOB",             value: "02-July-2000"),
                                            CustomMyInfo.Privacy.object(title: "Mobile",          value: "012-888-999"),
                                            CustomMyInfo.Privacy.object(title: "Organization",    value: "YG"),
                                            CustomMyInfo.Privacy.object(title: "Department",      value: "Mobile R&D"),
                                            
                                          ])
        
        
        // MARK: - Init Bizpoint -
        bizpointRec = CustomMyInfo.Bizpoint(
            title    : "BizPoint",
            subTitle : "Jisoo님의 biz 포인트를 자유롭게 사용할 수 있어요!",
            object   : [CustomMyInfo.Bizpoint.object(title    : "내 biz 포인트. V",
                                                     bizPoint : "200,000 P")])
        
        
        // MARK: - Init CancelReceipt -
        var object_rec_cancel_receipt          = [CustomMyInfo.CancelReceipt.object]()
        
        object_rec_cancel_receipt.append(
            CustomMyInfo.CancelReceipt.object(
                name         : "Cola",
                price        : "4000000 원",
                date         : "10월 20일(주)",
                id           : "기업1234",
                objectDetail :
                    CustomMyInfo.CancelReceipt.object.objectDetail(
                        approval_number : "3.14159",
                        supply_amount   : "10000원",
                        vat             : "200원"))
        )
        
        // other style to append data
        object_rec_cancel_receipt.append(
            CustomMyInfo.CancelReceipt.object(name         : "Pepsi",
                                              price        : "6000000 원",
                                              date         : "11월 23일(주)",
                                              id           : "기업567",
                                              objectDetail :
                                                CustomMyInfo.CancelReceipt.object.objectDetail(
                                                    approval_number : "9999",
                                                    supply_amount   : "20000원",
                                                    vat             : "500원"))
        )
        
      
        cancelReceiptRec = CustomMyInfo.CancelReceipt(title: "CancelReceipt", object: object_rec_cancel_receipt)
        
        
        // MARK: - Assign all data  -
        myInfoData = [
            CustomMyInfoData(title: MyInfoSection.Privacy.rawValue,       value: privacyRec),
            CustomMyInfoData(title: MyInfoSection.BizPoint.rawValue,      value: bizpointRec),
            CustomMyInfoData(title: MyInfoSection.CancelReceipt.rawValue, value: cancelReceiptRec),
            
        ]
    }
}
